$(document).ready(function() {
    // Initial page load
    loadPage('home');

    // Handle navigation link clicks
    $('a.nav-link').click(function(event) {
        event.preventDefault();
        let page = $(this).data('page');
        loadPage(page);
    });

    // Function to load content via AJAX
    function loadPage(page) {
        $.ajax({
            url: `${page}.html`, // Assuming your content files are named home.html, about.html, etc.
            type: 'GET',
            dataType: 'html',
            success: function(data) {
                $('#content').html(data);
            },
            error: function(xhr, status, error) {
                console.error('Error loading page:', error);
            }
        });
    }
});
