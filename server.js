// server.js

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config(); // Load environment variables from a .env file

const app = express();
const port = process.env.PORT || 3000;

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// MongoDB Connection
const mongoURI = process.env.MONGODB_URI; // MongoDB connection string from .env file
mongoose.connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => console.log('Connected to MongoDB'));

// MongoDB Schema
const InternshipApplicationSchema = new mongoose.Schema({
    name: String,
    email: String,
    Qualification: String,
    Institution: String,
    office: String,
    amount: String,
    message: String
});

const InternshipApplication = mongoose.model('InternshipApplication', InternshipApplicationSchema);

// Route to handle form submission
app.post('/submit-form', async (req, res) => {
    try {
        const formData = req.body;
        const newApplication = new InternshipApplication(formData);
        await newApplication.save();
        res.status(201).json({ message: 'Application submitted successfully' });
    } catch (err) {
        console.error('Error:', err);
        res.status(500).json({ error: 'Server error' });
    }
});

// Start server
app.listen(port, () => console.log(`Server running on port ${port}`));
